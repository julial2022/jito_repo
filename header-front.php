<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php wp_head(); ?>
</head>

<?php
	$navbar_scheme   = get_theme_mod( 'navbar_scheme', 'navbar-light bg-light' ); // Get custom meta-value.
	$navbar_position = get_theme_mod( 'navbar_position', 'static' ); // Get custom meta-value.

	$search_enabled  = get_theme_mod( 'search_enabled', '1' ); // Get custom meta-value.
?>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<a href="#main" class="visually-hidden-focusable"><?php esc_html_e( 'Skip to main content', 'jito' ); ?></a>

<div id="wrapper">
	<header style="height: 600px;
    background-color: #0a0026 !important;
    background: url(/jito/wp-content//themes/jito/img/AdobeStock_257818105_red.jpg) no-repeat center center;
    background-size: contain;">
	<div class="container">
				<a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php
						$header_logo = get_theme_mod( 'header_logo' ); // Get custom meta-value.

						if ( ! empty( $header_logo ) ) :
					?>
						<img src="<?php echo esc_url( $header_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
					<?php
						else :
							echo esc_attr( get_bloginfo( 'name', 'display' ) );
						endif;
					?>
				</a>
				<button type="button" class="btn btn-primary btn-grad ">Stay connected</button>

				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="<?php esc_attr_e( 'Toggle navigation', 'jito' ); ?>">
					<span class="navbar-toggler-icon"></span>
				</button>

				
			</div><!-- /.container -->
		<div class="container">
		<nav id="header" class="navbar navbar-expand-md <?php echo esc_attr( $navbar_scheme ); if ( isset( $navbar_position ) && 'fixed_top' === $navbar_position ) : echo ' fixed-top'; elseif ( isset( $navbar_position ) && 'fixed_bottom' === $navbar_position ) : echo ' fixed-bottom'; endif; if ( is_home() || is_front_page() ) : echo ' home'; endif; ?>">
				<div id="navbar" class="collapse navbar-collapse">
					<?php
						// Loading WordPress Custom Menu (theme_location).
						wp_nav_menu(
							array(
								'menu_class'     => 'navbar-nav me-auto',
								'container'      => '',
								'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
								'walker'         => new WP_Bootstrap_Navwalker(),
								'theme_location' => 'main-menu',
							)
						);

						if ( '1' === $search_enabled ) :
					?>
							<form class="search-form my-2 my-lg-0" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<div class="input-group">
									<input type="text" name="s" class="form-control" placeholder="<?php esc_attr_e( 'Search', 'jito' ); ?>" title="<?php esc_attr_e( 'Search', 'jito' ); ?>" />
									<button type="submit" name="submit" class="btn btn-outline-secondary"><?php esc_html_e( 'Search', 'jito' ); ?></button>
								</div>
							</form>
					<?php
						endif;
					?>
				</div><!-- /.navbar-collapse -->
				
		</nav><!-- /#header -->
			<div class="col-sm-6" style="position: relative;    top: 70px;color:#fff">
			<h1>Lorem ipsum dolor sit amet consectetur.</h1>
			<p>Lorem ipsum dolor sit amet consectetur.</p>
			<button type="button" class="btn btn-primary">Know More...</button>
			</div>
		</div>
	</header>


	<div class="container" style="top: -70px; position: relative;">
		<div class="row">
		<div class="col-sm-3">
				<div class="card">

					<div class="card-body">
						
						<img src="http://localhost:81/jito/wp-content/uploads/2023/09/placehlder.png" class="img-thumbnail" alt="place holder" width="304" height="236"> 
						<p class="card-text">Some example text. Some example text.</p>

					</div>
					<div class="card-footer">
						<a href="#" class="card-link">Card link</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="card">

					<div class="card-body">
						
						<p class="card-text">Some example text. Some example text.</p>
						
					</div>
					<div class="card-footer">
						<a href="#" class="card-link">Card link</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="card">

					<div class="card-body">
						
						<p class="card-text">Some example text. Some example text.</p>
						
					</div>
					<div class="card-footer">
						<a href="#" class="card-link">Card link</a>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="card">

						<div class="card-body">
							
							<p class="card-text">Some example text. Some example text.</p>
							
						</div>
						<div class="card-footer">
							<a href="#" class="card-link">Card link</a>
						</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
	<h2> Who we are?</h2>
		<div class="row">
    		<div class="col-sm-4">
				<div class="card">
					<div class="card-header">Header</div>
					<div class="card-body">Content</div>
					<div class="card-footer">Footer</div>
				</div>
			</div>
		<div class="col-sm-4">
		<div class="card">
					<div class="card-header">Header</div>
					<div class="card-body">Content</div>
					<div class="card-footer">Footer</div>
				</div>
		</div>
		<div class="col-sm-4">
		<div class="card">
					<div class="card-header">Header</div>
					<div class="card-body">Content</div>
					<div class="card-footer">Footer</div>
				</div>
		</div>
		</div>
	</div>





	<main id="main" class="container"<?php if ( isset( $navbar_position ) && 'fixed_top' === $navbar_position ) : echo ' style="padding-top: 100px;"'; elseif ( isset( $navbar_position ) && 'fixed_bottom' === $navbar_position ) : echo ' style="padding-bottom: 100px;"'; endif; ?>>
		<?php
			// If Single or Archive (Category, Tag, Author or a Date based page).
			if ( is_single() || is_archive() ) :
		?>
			<div class="row">
				<div class="col-md-8 col-sm-12">
		<?php
			endif;
		?>
